﻿using Microsoft.EntityFrameworkCore;
using ServerMoney.Model;
using System;
using System.Text;
using System.Text.RegularExpressions;

namespace ServerMoney.Context
{
    class MyAppContext : DbContext
    {
        public DbSet<User> Users => Set<User>();
        public DbSet<Payment> Payments => Set<Payment>();
        public DbSet<Category> Categories => Set<Category>();
        public MyAppContext() 
        { 
            //Database.EnsureDeleted(); 
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=ServerMoney.db");
        }

        public string GetConsoleUsers()
        {
            StringBuilder str = new StringBuilder();
            str.Append("Список пользователей\n");
            //Console.WriteLine("Список пользователей");
            foreach (var u in Users)
            {
                str.Append($"{GetSomeProperty<User>(u)}\n");
                //Console.WriteLine();
            }
            str.Append("\n");
            //Console.WriteLine();
            return str.ToString();
        }
        public string GetConsolePayments()
        {
            StringBuilder str = new StringBuilder();
            str.Append("Список расходов\n");
            //Console.WriteLine("Список расходов");
            foreach (var p in Payments)
            {
                str.Append($"{GetSomeProperty<Payment>(p)}\n");
                //Console.WriteLine();
            }
            str.Append("\n");
            //Console.WriteLine();
            return str.ToString();
        }
        public string GetConsoleCategories()
        {
            StringBuilder str = new StringBuilder();
            str.Append("Список категорий\n");
            //Console.WriteLine("Список категорий");
            foreach (var c in Categories)
            {
                str.Append($"{GetSomeProperty<Category>(c)}\n");
                //Console.WriteLine();
            }
            str.Append("\n");
            //Console.WriteLine();
            return str.ToString();
        }
        private string GetSomeProperty<T>(T t)
        {
            StringBuilder str = new StringBuilder();
            foreach (var prop in t.GetType().GetProperties())
            {
                str.Append($"{prop.Name}: {prop.GetValue(t)} ");
                //Console.Write(prop.Name + ": " + prop.GetValue(t) + " ");
            }
            return str.ToString();
        }
        public void AddNewUser(string str, string id)
        {
            Users.Add(new User { Name = str, Status = 0, IdTg = id });
            SaveChanges();
        }
        public void DeleteUser(int id)
        {
            User user = Users.Find(id);
            Users.Remove(user);
            SaveChanges();
        }
        public void AddNewCategory(string str)
        {
            Categories.Add(new Category { Name = str, Totalprice = 0 });
            SaveChanges();
        }
        public void DeleteCategory(int id)
        {
            Category cat = Categories.Find(id);
            Categories.Remove(cat);
            SaveChanges();
        }
        public void AddNewPayment(string str, int user, string cat)
        {
            int price = int.Parse(new Regex(@"\D").Replace(str, ""));
            //Console.WriteLine(price);

            string STR = new Regex(@"\d").Replace(str, "").Trim();
            //Console.WriteLine(STR);

            int icat = 0;
            foreach(var c in Categories)
            {
                if (c.Name == cat)
                {
                    icat = c.Id;
                    c.Totalprice += price;
                }
            }

            Payments.Add(new Payment { Price = price, Comment = STR, Date = DateTime.Now.ToString(), UserPayment = user, CategoryPay = icat });
            SaveChanges();
        }
        public void DeletePayment(int id)
        {
            Payment pay = Payments.Find(id);
            Payments.Remove(pay);
            SaveChanges();
        }
        public void Restart()
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
        }
    }
}
