﻿
namespace ServerMoney.Model
{
    class Payment
    {
        public int Id { get; set; }
        public int UserPayment { get; set; }
        public int CategoryPay { get; set; }
        public int Price { get; set; }
        public string Date { get; set; }
        public string Comment { get; set; }
    }
}
