﻿using System;
using System.Linq;
using System.Collections.Generic;
using ServerMoney.Context;
using System.Text;
using Telegram.Bot;
using Telegram.Bot.Extensions.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using System.Threading;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using ServerMoney.Model;

namespace ServerMoney
{
    class TGBot
    {
        public class TGbot
        {
            readonly ITelegramBotClient bot = new TelegramBotClient("5331529850:AAE1D3m3ZLBVZVm1vqsmA_WlnuhOe_4_9m4");
            bool NewCategory = false;
            bool NewUser = false;
            string NewPayment;

            public TGbot() { }
            public void Start()
            {
                Console.WriteLine("Запущен бот " + bot.GetMeAsync().Result.FirstName);

                var cts = new CancellationTokenSource();
                var cancellationToken = cts.Token;
                var receiverOptions = new ReceiverOptions
                {
                    AllowedUpdates = { },
                };
                bot.StartReceiving(
                    HandleUpdateAsync,
                    HandleErrorAsync,
                    receiverOptions,
                    cancellationToken
                );
                Console.ReadLine();
                cts.Cancel();
            }
            private async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
            {
                if (update.Type == UpdateType.Message && update?.Message?.Text != null)
                {
                    //Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(update));
                    Console.WriteLine($"{{From: {update.Message.Chat.Id}, {update.Message.From.FirstName}. Message: {update.Message.Text}}}");
                    await HandleMessage(botClient, update.Message);
                    return;
                }

                if (update.Type == UpdateType.CallbackQuery)
                {
                    await HandleCallbackQuery(botClient, update.CallbackQuery);
                    return;
                }
            }
            private async Task HandleMessage(ITelegramBotClient botClient, Message message)
            {
                if (NewCategory)
                {
                    using (MyAppContext db = new MyAppContext())
                    {
                        foreach(var c in db.Categories)
                        {
                            if (c.Name.ToLower().Trim() == message.Text.ToLower().Trim() || message.Text.Trim() == ".")
                            {
                                NewCategory = false;
                                await botClient.SendTextMessageAsync(message.Chat, "Отмена добавления");
                                return;
                            }
                        }
                        db.AddNewCategory(message.Text);
                    }
                    NewCategory = false;
                    await botClient.SendTextMessageAsync(message.Chat, "Новая категория добавлена");
                    return;
                }
                else if (NewUser)
                {
                    using (MyAppContext db = new MyAppContext())
                    {
                        db.AddNewUser(message.Text, message.From.Id.ToString());
                    }
                    NewUser = false;
                    await KeyboardPrint(botClient, message);
                    return;
                }

                if (message.Text.ToLower() == "/start")
                {
                    NewUser = true;
                    await botClient.SendTextMessageAsync(message.Chat, "Введите имя нового пользователя");
                    return;
                }
                else if (message.Text.ToLower() == "/dbprint")
                {
                    await PrintAllDb(botClient, message);
                    return;
                }
                else if (message.Text.ToLower() == "/restart")
                {
                    await Restart(botClient, message);
                    return;
                }
                else if (message.Text.ToLower().StartsWith("/deleteuser"))
                {
                    try
                    {
                        await DeleteUser(botClient, message);
                    }
                    catch(Exception)
                    {
                        await botClient.SendTextMessageAsync(message.Chat, "Произошла ошибка");
                    }
                    return;
                }
                else if (message.Text.ToLower().StartsWith("/deletecategory"))
                {
                    try
                    {
                        await DeleteCategory(botClient, message);
                    }
                    catch (Exception)
                    {
                        await botClient.SendTextMessageAsync(message.Chat, "Произошла ошибка");
                    }
                    return;
                }
                else if (message.Text.ToLower().StartsWith("/deletepay"))
                {
                    try
                    {
                        await DeletePay(botClient, message);
                    }
                    catch (Exception)
                    {
                        await botClient.SendTextMessageAsync(message.Chat, "Произошла ошибка");
                    }
                    return;
                }
                else if (message.Text.ToLower() == "/новая категория")
                {
                    NewCategory = true;
                    await botClient.SendTextMessageAsync(message.Chat, "Введите название новой категории \nили \".\" чтобы отменить");
                    return;
                }
                else if (Char.IsDigit(message.Text[0]))
                {
                    await AddTgNewPayment(botClient, message);
                    return;
                }
                else if (message.Text.ToLower() == "/категории")
                {
                    await PrintCategories(botClient, message);
                    return;
                }
                else if (message.Text.ToLower() == "/расходы")
                {
                    await PrintPayments(botClient, message);
                    return;
                }
                else if (message.Text.ToLower() == "/аналитика")
                {
                    await Analize(botClient, message);
                    return;
                }
            }
            private async Task AddTgNewPayment(ITelegramBotClient botClient, Message message)
            {
                List<InlineKeyboardButton[]> list = new List<InlineKeyboardButton[]>();
                using (MyAppContext db = new MyAppContext())
                {
                    foreach (var s in db.Categories)
                    {
                        list.Add(new[] { InlineKeyboardButton.WithCallbackData(s.Name) });
                    }
                }
                var keyboard = new InlineKeyboardMarkup(list);
                NewPayment = message.Text;
                await botClient.SendTextMessageAsync(message.Chat.Id, "Выберите категорию:", replyMarkup: keyboard);
            }
            private async Task KeyboardPrint(ITelegramBotClient botClient, Message message)
            {
                ReplyKeyboardMarkup keyboard = new(new[]
                {
                new KeyboardButton[] {"/Расходы", "/Категории"},
                new KeyboardButton[] {"/Аналитика", "/Новая категория"},
                })
                {
                    ResizeKeyboard = true
                };
                await botClient.SendTextMessageAsync(message.Chat.Id, "Новый пользователь добавлен", replyMarkup: keyboard);
            }
            private async Task PrintAllDb(ITelegramBotClient botClient, Message message)
            {
                StringBuilder str = new StringBuilder();
                using (MyAppContext db = new MyAppContext())
                {
                    str.Append(db.GetConsoleUsers());
                    str.Append(db.GetConsoleCategories());
                    str.Append(db.GetConsolePayments());
                }
                await botClient.SendTextMessageAsync(message.Chat, str.ToString());
            }
            private async Task PrintCategories(ITelegramBotClient botClient, Message message)
            {
                StringBuilder str = new StringBuilder();
                using (MyAppContext db = new MyAppContext())
                {
                    str.Append("🔹Список категорий\n==========\n");
                    foreach (var s in db.Categories)
                    {
                        str.Append($"🔸{s.Name}\n");
                    }
                }
                await botClient.SendTextMessageAsync(message.Chat, str.ToString());
            }
            private async Task PrintPayments(ITelegramBotClient botClient, Message message)
            {
                StringBuilder str = new StringBuilder();
                using (MyAppContext db = new MyAppContext())
                {
                    str.Append("🔹Список расходов\n==========\n");
                    foreach (var c in db.Categories.OrderByDescending(n => n.Totalprice))
                    {
                        str.Append($"🔸{c.Name}\n");
                        foreach(var p in db.Payments.Where(x => x.CategoryPay == c.Id))
                        {
                            str.Append($"➖{p.Price}р {db.Users.Find(p.UserPayment).Name} {p.Date.Substring(0, 5)} {p.Comment}\n");
                        }
                    }
                }
                await botClient.SendTextMessageAsync(message.Chat, str.ToString());
            }
            private async Task Analize(ITelegramBotClient botClient, Message message)
            {
                StringBuilder str = new StringBuilder();
                using (MyAppContext db = new MyAppContext())
                {
                    int total = 0;
                    foreach (var i in db.Payments)
                    {
                        total += i.Price;
                    }
                    str.Append($"📊Аналитика расходов\n==========\n");
                    foreach (var i in db.Categories.OrderByDescending(n => n.Totalprice))
                    {
                        int tmp = i.Totalprice;
                        double perc = (double)tmp / (double)total * 100;
                        perc = Math.Round(perc);
                        if (total == 0)
                            perc = 0;
                        str.Append($"➖{i.Name} {tmp}р ({perc}%)\n");
                    }
                }
                await botClient.SendTextMessageAsync(message.Chat.Id, str.ToString());
                return;
            }
            private async Task  DeleteUser(ITelegramBotClient botClient, Message message)
            {
                int id = int.Parse(new Regex(@"\D").Replace(message.Text, ""));
                using (MyAppContext db = new MyAppContext())
                {
                    foreach(var p in db.Payments)
                    {
                        if (p.UserPayment == id)
                        {
                            await botClient.SendTextMessageAsync(message.Chat.Id, "Пользователь не может быть удален");
                            return;
                        }
                    }
                    db.DeleteUser(id);
                }
                await botClient.SendTextMessageAsync(message.Chat.Id, "Пользователь удален");
                return;
            }
            private async Task DeleteCategory(ITelegramBotClient botClient, Message message)
            {
                int id = int.Parse(new Regex(@"\D").Replace(message.Text, ""));
                using (MyAppContext db = new MyAppContext())
                {
                    foreach (var p in db.Payments)
                    {
                        if (p.CategoryPay == id)
                        {
                            await botClient.SendTextMessageAsync(message.Chat.Id, "Категория не может быть удалена");
                            return;
                        }
                    }
                    db.DeleteCategory(id);
                }
                await botClient.SendTextMessageAsync(message.Chat.Id, "Категория удалена");
                return;
            }
            private async Task DeletePay(ITelegramBotClient botClient, Message message)
            {
                int id = int.Parse(new Regex(@"\D").Replace(message.Text, ""));
                using (MyAppContext db = new MyAppContext())
                {
                    Payment tmp = db.Payments.Find(id);
                    db.Categories.Find(tmp.CategoryPay).Totalprice -= tmp.Price;
                    db.DeletePayment(id);
                }
                await botClient.SendTextMessageAsync(message.Chat.Id, "Расход удален");
                return;
            }
            private async Task Restart(ITelegramBotClient botClient, Message message)
            {
                using (MyAppContext db = new MyAppContext())
                {
                    db.Restart();
                }
                await botClient.SendTextMessageAsync(message.Chat.Id, "Все данные удалены\nДля начала работы введите /start");
                return;
            }
            private async Task HandleCallbackQuery(ITelegramBotClient botClient, CallbackQuery callbackQuery)
            {
                await botClient.EditMessageReplyMarkupAsync(callbackQuery.Message.Chat.Id, callbackQuery.Message.MessageId);
                using (MyAppContext db = new MyAppContext())
                {
                    var idUser = db.Users.FirstOrDefault(p => p.IdTg == callbackQuery.Message.Chat.Id.ToString());
                    db.AddNewPayment(NewPayment, idUser.Id, callbackQuery.Data);
                }
                await botClient.SendTextMessageAsync(callbackQuery.Message.Chat.Id, "Новый расход добавлен");
                return;
            }
            private Task HandleErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
            {
                Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(exception));
                return Task.CompletedTask;
            }
        }
    }
}
